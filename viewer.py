# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QAction, QFileDialog, QApplication

import os as os
from os.path import expanduser

from pyqtgraph import plot
from pyqtgraph import PlotWidget as QGraphWidget
import pyqtgraph as pg
import json
from utils import *

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):

        # UTILS
        def fileDialog():
            default_folder = expanduser("~")
            filter = "ASCII TXT (*.txt) ;; JSON (*.json)"
            fname = QFileDialog.getOpenFileName(self, 'Open file', default_folder, filter)

            if fname[0]:
                return(fname[0])

        def folderDialog():
            default_folder = expanduser("~")
            fname = QFileDialog.getExistingDirectory(self, 'Open Folder', default_folder)
            if fname:
                return(fname)

        # MAIN

        # COLLECT LINES
        #
        # spectra_lines = [
        #   {
        #       'field': np.array([...]),
        #       'intensity': np.array([...]),
        #       'filename': filename,
        #       'color': getColor() or something,
        #       'id': len(spectra_lines) -1,
        #       'view': Booleana (legata alla checkbox)
        #   }
        # ]

        spectra_lines = []

        def plotSpectra(sp_array):

            # Style Settings
            # self.graphWidget.setBackground('#EBEBEB') #ggplot useful
            self.graphWidget.setLabel('left', 'Intensity', color='w') # Intensity in ordinate
            self.graphWidget.setLabel('bottom', 'Field', color='w') # Field in ascisse

            # Drawer
            first_draw = True

            for sp in sp_array:
                if bool(sp['view']): # Draw only view: True spectra
                    pen = pg.mkPen(
                        color = sp['color'], # Take colour from sp['color']
                        width = 1,
                    )
                    self.graphWidget.plot(sp['field'], sp['intensity'], pen=pen, clear=first_draw)
                    first_draw = False

        def compileTable(sp_array, table):
            columns = ['filename', 'color', 'date', 'comment', 'points']

            table.setColumnCount(len(columns)) # Set table column number
            table.setHorizontalHeaderLabels(columns) # Give labels to horiz. labels
            table.verticalHeader().hide() # Hide vertical labels
            table.setRowCount(0) # Reset table. From the docs: "If this is less than rowCount(), the data in the unwanted rows is discarded."

            for sp in sp_array:
                currentRowCount = table.rowCount()
                table.setRowCount(currentRowCount + 1) # Set table rows

                for column in columns:
                    table.setItem(currentRowCount, columns.index(column), QtGui.QTableWidgetItem(str(sp[column])))

                filename = table.item(currentRowCount, columns.index('filename')) # Get filename element
                filename.setFlags(filename.flags() | QtCore.Qt.ItemIsUserCheckable) # Add checkbox to filename element

                if bool(sp['view']):
                    filename.setCheckState(QtCore.Qt.Checked) # Set as Checked/Unchecked, basing on sp['view']
                else:
                    filename.setCheckState(QtCore.Qt.Unchecked)

        def openFile():
            file_path = str(fileDialog()) # Get file with a Dialog
            path, filename = os.path.split(file_path) # Get directory and filename as distinct vars
            filepure = str(os.path.splitext(filename)[0]) # Get file name without extension

            try:
                if filename.endswith('json'):
                    file = open(file_path,'r')
                    spectrum = json.loads(file.read()) # Load from string
                    spectrum = todict(spectrum, filename, spectra_lines)
                    file.close()
                    self.lbl_status.setText('File loaded!')
                elif filename.endswith('txt'):
                    spectrum = collectFromAscii(file_path)
                    spectrum = todict(spectrum, filename, spectra_lines) # diminuire le variabili in ingresso, magari
            except Exception as e:
                self.lbl_status.setText('Error in file reading. Try again or select another file\n¯\\_(ツ)_/¯')

            spectra_lines.append(spectrum)
            compileTable(spectra_lines, self.table_files)
            plotSpectra(spectra_lines)

        def updateSettings():
            sp_array = spectra_lines
            table = self.table_files

            for row in range(0, table.rowCount()): # for row in table rows
                item = table.item(row, 0) #  Get item in the first column
                if item.checkState() == 2:
                    sp_array[row]['view'] = True
                else:
                    sp_array[row]['view'] = False

            plotSpectra(spectra_lines)

        def listeners():
            self.btn_selectFile.clicked.connect(openFile)
            self.btn_plot.clicked.connect(self.update)
            self.btn_update.clicked.connect(updateSettings)

            # Actions
            # self.actionReset_Input_Boxes.triggered.connect(resetInputs)
            self.actionQuit.triggered.connect(QApplication.quit)

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(744, 566)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.lbl_title_2 = QtWidgets.QLabel(self.centralwidget)
        self.lbl_title_2.setMaximumSize(QtCore.QSize(16777215, 20))
        self.lbl_title_2.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_title_2.setObjectName("lbl_title_2")
        self.gridLayout.addWidget(self.lbl_title_2, 2, 0, 1, 1)
        self.group_panel = QtWidgets.QGroupBox(self.centralwidget)
        self.group_panel.setMaximumSize(QtCore.QSize(16777215, 300))
        self.group_panel.setTitle("")
        self.group_panel.setObjectName("group_panel")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.group_panel)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.groupBox_spectra = QtWidgets.QGroupBox(self.group_panel)
        self.groupBox_spectra.setMaximumSize(QtCore.QSize(500000, 16777215))
        self.groupBox_spectra.setObjectName("groupBox_spectra")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.groupBox_spectra)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.table_files = QtWidgets.QTableWidget(self.groupBox_spectra)
        self.table_files.setObjectName("table_files")
        self.table_files.setColumnCount(0)
        self.table_files.setRowCount(0)
        self.gridLayout_3.addWidget(self.table_files, 1, 0, 1, 1)
        self.gridLayout_4.addWidget(self.groupBox_spectra, 0, 0, 1, 1)
        self.lbl_status = QtWidgets.QLabel(self.group_panel)
        self.lbl_status.setMaximumSize(QtCore.QSize(16777215, 60))
        self.lbl_status.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl_status.setObjectName("lbl_status")
        self.gridLayout_4.addWidget(self.lbl_status, 1, 0, 1, 2)
        self.groupBox_btns = QtWidgets.QGroupBox(self.group_panel)
        self.groupBox_btns.setMinimumSize(QtCore.QSize(150, 0))
        self.groupBox_btns.setMaximumSize(QtCore.QSize(200, 16777215))
        self.groupBox_btns.setTitle("")
        self.groupBox_btns.setObjectName("groupBox_btns")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.groupBox_btns)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.btn_selectFile = QtWidgets.QPushButton(self.groupBox_btns)
        self.btn_selectFile.setObjectName("btn_selectFile")
        self.gridLayout_5.addWidget(self.btn_selectFile, 1, 0, 1, 1)
        self.btn_update = QtWidgets.QPushButton(self.groupBox_btns)
        self.btn_update.setObjectName("btn_update")
        self.gridLayout_5.addWidget(self.btn_update, 2, 0, 1, 1)
        self.btn_plot = QtWidgets.QPushButton(self.groupBox_btns)
        self.btn_plot.setMaximumSize(QtCore.QSize(182, 16777215))
        self.btn_plot.setObjectName("btn_plot")
        self.gridLayout_5.addWidget(self.btn_plot, 3, 0, 1, 1)
        self.gridLayout_4.addWidget(self.groupBox_btns, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.group_panel, 1, 0, 1, 1)
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.graphWidget = QGraphWidget(self.frame)
        self.graphWidget.setObjectName("graphWidget")
        self.gridLayout_2.addWidget(self.graphWidget, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.frame, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 744, 20))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionQuit = QtWidgets.QAction(MainWindow)
        self.actionQuit.setObjectName("actionQuit")
        self.actionReset = QtWidgets.QAction(MainWindow)
        self.actionReset.setObjectName("actionReset")
        self.actionSave_status = QtWidgets.QAction(MainWindow)
        self.actionSave_status.setObjectName("actionSave_status")
        self.actionLoad_status = QtWidgets.QAction(MainWindow)
        self.actionLoad_status.setObjectName("actionLoad_status")
        self.menuFile.addAction(self.actionQuit)
        self.menuFile.addAction(self.actionReset)
        self.menuFile.addAction(self.actionSave_status)
        self.menuFile.addAction(self.actionLoad_status)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        listeners() ## Mine

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "ESR Viewer"))
        self.lbl_title_2.setText(_translate("MainWindow", "ESR Viewer was </> with <3 by Giovanni Crisalfi"))
        self.groupBox_spectra.setTitle(_translate("MainWindow", "Spectra"))
        self.lbl_status.setText(_translate("MainWindow", "Ready."))
        self.btn_selectFile.setText(_translate("MainWindow", "Select File"))
        self.btn_update.setText(_translate("MainWindow", "Update"))
        self.btn_plot.setText(_translate("MainWindow", "Plot"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.actionQuit.setText(_translate("MainWindow", "Quit"))
        self.actionReset.setText(_translate("MainWindow", "Reset"))
        self.actionSave_status.setText(_translate("MainWindow", "Save status"))
        self.actionLoad_status.setText(_translate("MainWindow", "Load status"))
