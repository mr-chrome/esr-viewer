from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtWidgets import QApplication
import sys
import viewer
import os

import numpy as np
import pylab
import time
import pyqtgraph as pg

class viewerApp(QtWidgets.QMainWindow, viewer.Ui_MainWindow):
    def __init__(self, parent=None):
        super(viewerApp, self).__init__(parent)
        self.setupUi(self)

    def update(self):
        t1=time.clock()
        points=100
        X=np.arange(points)
        Y=np.sin(np.arange(points)/points*3*np.pi+time.time())
        C=pg.hsvColor(time.time()/5%1,alpha=.5)
        pen=pg.mkPen(color=C,width=2)
        self.graphWidget.plot(X,Y,pen=pen,clear=True)
        # self.graphWidget.setBackground((100,50,255,0))   # Set transparency
        # print("update took %.02f ms"%((time.clock()-t1)*1000))
        if True:
            QtCore.QTimer.singleShot(1, self.update) # QUICKLY repeat

def main():
    app = QApplication(sys.argv)
    form = viewerApp()
    form.show()
    app.exec_()

if __name__ == '__main__':
    main()
