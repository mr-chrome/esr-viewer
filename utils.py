# TODOS:
# - Collect from ESR, implementing auto-generation of f_field array, like in original ESR Commander

import json
import numpy as np

def collectFromAscii(inpath):

    dataset = { 'field': [], 'intensity': [] } # Collect data in a dictionary
    f = open(inpath, 'r') # Open the original ASCII file

    # For each line in the file:
    for line in f:
        line = line.strip() # Elimina caratteri invisibili
        columns = line.split() # Make a list from the string
        if len(columns) == 3:
            dataset['field'].append(columns[1])
            dataset['intensity'].append(columns[2])

    f.close()
    return dataset

def colorPicker(sp_array):
    # TODO: integrate input from table QtGui.QColor(sp['color'])
    color_list = ['r', 'g', 'y']

    if len(sp_array) == 0:
        return(color_list[0])

    last_color = sp_array[len(sp_array) - 1]['color']
    last_index = color_list.index(last_color)

    if len(color_list) == last_index:
        return color_list[0]
    else:
        return color_list[last_index + 1]

def todict(spectrum, filename, spectra_lines):
    dict = {}

    dict['filename'] = filename
    dict['view'] = True

    if 'date' in spectrum.keys():
        dict['date'] = spectrum['date']
    else:
        dict['date'] = 'No date'

    if 'comment' in spectrum.keys():
        dict['comment'] = spectrum['comment']
    else:
        dict['comment'] = 'No comment'

    dict['color'] = colorPicker(spectra_lines)
    dict['points'] = int(len(spectrum['intensity']))

    dict['field'] = np.array(spectrum['field']).astype(np.float)
    dict['intensity'] = np.array(spectrum['intensity']).astype(np.float)

    return dict
